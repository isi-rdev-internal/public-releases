# [2020.10.05 Kukabildo](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md)

## Changelogs

### Features
* Auto Use of Leave Credits
* Automatic carry over of leave credits
* Automatic leave crediting (monthly vacation leave credits and yearly sick leave credits)
* Biometrics real-time notifications
* Custom Global Notification
* Custom Employee Profile Details
* Employee Profile Updates Tracking
* Add Project Categories
* Night Shift Time Log
* Overtime and Leave Team Leader Approval
* Overtime and Leave Bulk HR Email Notification
* Payslip Email Sending

### Enhancements
* Biometrics: Verify Hours Reports
* Add notes in timer and add start button
* Update validation for the field ‘hours’ in Timesheet Form
* UI enhancements for Overtime and Leave mobile view
* UI Enhancements for Timesheet Week and Day View
* Separate Submit and Save Draft button in Overtime and Leave request forms
* Replace toggle switch with check/ex icons as with/without pay indicator (Leave form)
* Add new option “All Approved” in Overtime downloads
* Leave/Overtime Drafts Warning Notification
* Set Overtime Type of Overtime Request

### Fixes
* Duplicate Holiday Notification
* Some entries appear in week view which are not in day view when navigating to previous weeks
* Can apply Overtime for past dates
* A warning appears when updating hours in week view
* Timer Button doesnt hide upon changing date (Timesheet Form)
* Payroll Timesheet Notes unicode issue

# [2020.03.12 Tagohala](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md)

## Downloads
* [Innovuze Tracker-2020.3.12.dmg](https://tracker.innovuze:4324/static/releases/Innovuze%20Tracker-2020.3.12.dmg) (MacOS)
* [Innovuze Tracker Setup 2020.3.12.exe](https://tracker.innovuze:4324/static/releases/Innovuze%20Tracker%20Setup%202020.3.12.exe) (Windows Installer)
* [Innovuze Tracker-2020.3.12-x86_64.AppImage](https://tracker.innovuze:4324/static/releases/innovuze-tracker-2020.3.12-x86_64.AppImage) (Linux AppImage)

## Changelogs

### Features
* Automated Vacation Leave/Sick Leave application
* Automated Overtime application

### Enhancements
* Tasks sub-category
* Log entry timer

# [2019.06.27 Iniguihan](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md)

## Changelogs

### Features
* Excess break notification for the current quincena
* Time log with excess **[Break]** tasks will prompt a notification and will not continue

### Enhancements
* Column selection for Payroll timesheet download

### Bugfixes
* Time log details reverted during error
* Time log shows on the current selected day in day view when set to a different week with similar day
* Changes _(updating or deleting time log)_ does not reflect when switching from day to week
* Log total not updated when deleting time log _(and switching from day to week)_
* Disappearing holidays if two (2) holidays are in the same week
* Users with comma in their profile names cannot download time log reports _(ie. Johnny delaCruz, Jr.)_

# [2018.11.08 Tulukibon](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (11/08/2018)

## Changelogs

### Enhancements
* Copy previous week tasks to current week implementation
* Remember last time log entry selections
* Add copy project feature
* Set to current date when transitioning from Week to Day view
* Set all date formatting to mm/dd/yyyy
* Arrange holidays by date
* Rename DTR Reports column to match with the excel file
* Add client access to reports
* Logged time reporting

### Bugfixes
* Enclose routes with BrowserRouter to enable Back button navigation
* Add explicit redirection on successful password change
* Push search routes directly to browser history
* Fix Holidays sort on exported CSV file
* Clear start/end time field on invalid values



# [2018.08.08 Hanutoy](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (MKG)

## Changelogs

### Enhancements
* Unsynched Harvest/holiday notification
* Hide app when close button is clicked on MacOS

### Bugfixes
* Session expiry timeout warning negative countdown


# [3.1.0-rc Pinahiyom](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (07/30/2018)

## Changelogs

### Enhancements
* Allow time logging (Harvest only) even if not wired (no internet access)
* Delete button UI aesthetics
* Allow simple time logging calculations
* Retain week view rows on empty entries

# [3.0.0-rc Ngilngiga](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (07/09/2018)

## Changelogs

### Bugfixes
* Holiday notif entry doesn't disappear on Harvest fetch
* Time records reports permission doesn't work
* Holiday efficiency in DTR reports
* Task items missing if similar task is previously selected in week view

### Enhancements
* One-click day/week time log add/delete
* Delete feature now available in time log modal
* Added resigned field
* Persistent credentials (remember me?)
* Session expiry warning and automatic log out
* Additional UX/UI enhancements



# [2.0.0-rc Kulbahinam](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (04/11/2018)

## Changelogs

### Bugfixes
* App download progress box freezes on errors/connection issues
* Harvest admin accounts fetch all time logs
* Harvest token data cleared when updating current user details
* Inactive tasks/users visible in project task/user assignments
* Inconsistent holiday dates

### Enhancements
* Include user additional details (SSS, Pag-IBIG, TIN #, etc)
* Allow sub-teams for employees
* Collated page for reports




# [1.2.4-rc Nakuratan pt. 3](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (03/09/2018)

## Changelogs

### Bugfixes
* PATCH method fails in the current Nginx+Gunicorn production deployment setup

### Enhancements
* Apply Harvest-like time calculation/aggregation for time log entries


# [1.2.3-rc Nakuratan pt. 2](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (02/27/2018)

## Changelogs

### Regressions
* Decimal precision issue in time logs


# [1.2.2-rc Nakuratan](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (02/22/2018)

## Changelogs

### Bugfixes
* Decimal precision issue in time logs

### Enhancements
* Add Time-in / Time-out for payroll reports

# [1.2.1-rc Guipakuratan](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (02/20/2018)
Late minute (or days?) changes :)

## Changelogs

### Bugfixes
* Timesheet day log form state not reset
* Python 3.5.x on Tracker server fails with SSL protocol violation

### Enhancements
* 24-hour format support for start and end time


# [1.2.0-rc Pinakurat](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (02/16/2018)

## Changelogs

### Bugfixes
* Timesheet fetch only returns 20 entries in result set
* Field state values lost when verifying Harvest or updating profile
* No display (blank pages) when special permissions are set to certain users
* Time log Harvest timesheet id still referenced when changed to non-Harvest

### Enhancements
* Automatic detection on unsynched (deleted?) local Harvest 
entries
* Harvest API errors now properly handled
* Moving from day to week view now autofocus the selected day

### Features
* App and browser now runs in `https`
* Regular Holidays now automatically appear (for logging) in timesheet
* Downloads now handled by the native app (Electron)


# [1.0.1-rc Wayokuno](https://bitbucket.org/isi-rdev-internal/public-releases/src/master/release-info/innovuze-tracker.md) (02/05/2018)
This release merges the app packaging implementations to the main branch.

## Changelogs

### Bugfixes
* Fix for timesheet minute calculation discrepancies
* Totally remove Harvest icon for non-Harvest users; grayed Harvest icon for mix entries (local and Harvest time logs)

### Enhancements
* Teams and positions can now be added in the administration page
* Added app version checker
* App version now visible in the sidebar

### Features
* Context menu for input and textarea fields
* Retrieve and store window size and position
* Use of [`electron-builder`](https://github.com/electron-userland/electron-builder) for better app packaging
